迴圈
while，條件不符就一次也不會執行
判斷式在前面

repeat，條件不符也會執行一次
判斷式在後面

-----------------------------------------------

s:share 共享鎖
讀取資料時用 S 鎖

x:exclusive 排他鎖 / 獨佔鎖
修改資料時用 X 鎖

兩個鎖是互相排斥的，但有例外，有些資料庫可以讀取需要確認各家資料庫



-- 會超賣範例(勿用)
-- do sleep 是延遲執行 (在這只是為了模擬同時購買)
/*
DELIMITER $$

CREATE PROCEDURE buy(pid int)
BEGIN
	DECLARE n int;
    SELECT amount INTO n FROM product WHERE id = pid;
    DO sleep(20);
    IF n > 0 THEN
    	UPDATE product SET amount = amount - 1 WHERE id = pid;
        SELECT amount FROM product WHERE id = pid;
    ELSE
    	SELECT '賣完了' AS message;
    END IF;
    
END $$

DELIMITER ;
*/


-- 會超賣修改後(絕對不會超賣，可用)
/*
DELIMITER $$

CREATE PROCEDURE buy(pid int)
BEGIN
	DECLARE n int;
    START TRANSACTION;
    	UPDATE product SET amount = amount - 1 WHERE id = pid;
        SELECT amount INTO n FROM product WHERE id = pid;
    	DO sleep(10);
    	IF n >= 0 THEN
        	SELECT amount FROM product WHERE id = pid;
            COMMIT;
    	ELSE
    		SELECT '賣完了' AS message;
            ROLLBACK;
    	END IF;
    
END $$

DELIMITER ;
*/


--------------------------------------------------------------------
-- cursor

/*
DELIMITER $$

create PROCEDURE changer()
begin
	declare isDone bool default false;
	declare c_address varchar(200);
	declare c cursor for select address from house;
	declare continue handler for not found set isDone = true;

open c;
	fetch c into c_address;
	while !isDone DO
		if c_address like '台中縣%' then
		update house set address = REPLACE (address, '台中縣', '臺中市') where address like '台中縣%';
		end if;

		if c_address like '台%' then
		update house set address = REPLACE (address, '台', '臺') where address like '台%';
		end if;

		if c_address like '%臺中路%' then
		update house set address = REPLACE (address, '臺中路', '台中路') where address like '%臺中路%';
		end if;

		if c_address like '%豐原市%' then
		update house set address = REPLACE (address, '豐原市', '豐原區') where address like '%豐原市%';
		end if;

		fetch c into c_address;
	end while;
close c;

end $$

DELIMITER ;
*/
--------------------------------------------------------------------
使用者帳號
root 全線最大


-- 備份
u = user
abuser = 帳號
p = 密碼
1234 = 輸入密碼
B = 
R = 
addressbook = 資料庫名子
> = 輸出轉向(看你要轉到哪)
abbackup.sql = 儲存的檔案
mysqldump -u abuser -p 1234 -B -R -addressbook > abbackup.sql


-- 還原
打開 myadmin 匯入就好了