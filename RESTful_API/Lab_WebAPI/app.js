// 以 Express 建立 Web 伺服器
var express = require("express");
var app = express();

// 以 body-parser 模組協助 Express 解析表單與JSON資料
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Web 伺服器的靜態檔案置於 public 資料夾
app.use(express.static("public"));

// 以 express-session 管理狀態資訊
var session = require('express-session');
app.use(session({
    secret: 'secretKey',
    resave: false,
    saveUninitialized: true
}));

// 指定 esj 為 Express 的畫面處理引擎
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.set('views', __dirname + '/view');

// 一切就緒，開始接受用戶端連線
app.listen(3000);
console.log("Web伺服器就緒，開始接受用戶端連線.");
console.log("「Ctrl + C」可結束伺服器程式.");

let mysql = require('mysql');
let connection = mysql.createConnection({
    host: '127.0.0.1',
    password: '',
    user: 'root',
    port: 3306,
    database: 'labdb',
});

connection.connect(function (error) {
    if (error) {
        console.log(error.message);
    } else {
        console.log("ok");
    }
});

app.get('/home/news', function (req, res) {
    connection.query(
        'select * from news',
        function (error, data) {
            res.send(JSON.stringify(data));
        })
});

app.get('/lab/news/:min/:max', function (req, res) {
    connection.query(
        'select * from news where newsId between ? and ?',
        [req.params.min, req.params.max],
        function (error, data) {
            if (error) {
                console.log(error.message);
            } else {
                res.send("select");
            }
        })
});

app.post('/home/news', function (req, res) {
    connection.query(
        'insert into news (ymd, title) value (?, ?)',
        [req.body.ymd, req.body.title],
        function (error, result) {
            if (error) {
                console.log(error.message);
            } else {
                res.send("insert");
            }
        }
    )
});

app.put('/home/news', function (req, res) {
    connection.query(
        'update news set title = ?, ymd = ? where newsId = ?',
        [req.body.title, req.body.ymd, req.body.newsId],
        function (error, result) {
            if (error) {
                console.log(error.message);
            } else {
                res.send("update");
            }
        }
    )
});

app.delete('/home/news', function (req, res) {
    connection.query(
        'delete from news where newsId = ?',
        [req.body.newsId],
        function (error, result) {
            if (error) {
                console.log(error.message);
            } else {
                res.send("delete");
            }
        }
    )
});

app.get("/lab/ejs", function (req, res) {
    res.render("lab.ejs", { userName: "Jim" });
})