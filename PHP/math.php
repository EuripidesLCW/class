<?php
class Math {
    public static $pi = 3.14;

    static function add($a, $b) {
        return $a + $b;
    }

}

//設定static後 可以這樣用
// echo Math::$pi . "\n";
// echo Math::add(8, 15);

//設定static後 "屬性"不可以呼叫，但"function"可以被呼叫
// $m = new Math();
// echo $m->pi . "\n";
// echo $m->add(3, 4) . "\n";

//reference type $m3跟$m1會在同一個記憶體裡
//value type $m3會拷貝一份$m1記憶體，自己獨立出來
$m1 = new Math();
$m2 = new Math();
$m3 = $m1;

//對物件而言，兩個==就是比對內容，三個===比對記憶體
var_dump ($m1 == $m2);

?>