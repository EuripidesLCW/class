<?php
// echo "Hello, World";

//變數前要加$，字串中夾帶變數或跳脫字元時須使用雙引號
// $a = 10;
// echo "value is $a";

//非數值型態索引的陣列
// $arr = ["age"=>10, "name"=>"David"];
// echo $arr["age"];
// echo $arr["name"];

//型態轉換
// $a = 10;
// $a = (string)$a;
// var_dump($a);

//條件判斷
// if ($a > $b) {

// } elseif ($a == $b) {

// } else {

// }

//Switch Case
//記得要加break
// $i = 90;
// switch ($i) {
//     case $i >= 60:
//         echo "成績及格";
//         break;
//     case $i < 60 && $i >= 0:
//         echo "成績不及格";
//         break;
//     default:
//         echo "成績錯誤";
// }

//迴圈
//for
// for ($i = 1; $i <= 10; $i++) {
//     echo $i;
// }
// $i = 0;

//while
// while ($i <= 10) {
//     echo $i;
//     $i += 1;
// }

//表頭資料
//範例登入成功跳轉網頁
// $islogin = true;
// if ($islogin) {
//     header("location: welcome.php");
// } else {
//     header("location: fail.php");
// }

//停止程式
//exit 跟 die 是一樣的(比較常用die)，除錯用的(相當於alert)
// $islogin = true;
// if ($islogin) {
//     die("hello");
// } else {
//     header("location: fail.php");
// }

//字串
//多行文字輸出
// $str = <<<END
//     hi
//         hello
//     good morning
// END;
// echo "<pre>"; //加入pre標籤就不會被縮排
// echo $str;
// echo "</pre>";

//常用函數
//字串長度
//字串長度 英文1位元 中文3位元 表情符號4位元
// $s = "hello您好";
// echo strlen($s); //11
//這是幾個字的長度
// $s = "hello您好";
// echo strlen(utf8_decode($s)); //7 

//字串分割
//分割出來結果一定是陣列
// $s = "a,b,c";
// $arr = explode(",", $s);
// var_dump($arr)

//特殊字串轉換
//HTML用
// $s = "<hello>&\n<hi>";
// echo nl2br(htmlspecialchars($s)); // \n 配上 nl2br ，就會在\n的地方改成<br>

//中文子字串
// $s = "今天天氣晴";
// echo mb_substr($s, 2, 2, "utf-8"); //mb_substr 這樣才能照字下去抓

//字串搜尋
// $s = "今天天氣晴";
// echo strstr($s, "天氣"); //找到的後面全都會顯示
// echo var_dump(strstr($s, "天氣5")); //沒有找到會傳回false
// echo mb_strpos($s, "天氣", 0, "utf-8"); //搜尋位子 0是起始位子 utf-8要寫在第四個參數
// echo var_dump(mb_strpos($s, "天氣5", 0, "utf-8")); //找不到一樣傳回false

//表單
//讓前端後端接起來的東西
//GET 想要分享，傳輸資料量有限
//POST 想要把資料隱藏，傳輸資料量無限制
//REQUEST 不管前端設定get或post都可以抓，安全性差
// --------------------------
// 複合式抓取 GET + POST
// $id = $_GET["id"];
// $a = $_POST["a"];
// $b = $_POST["b"];
// --------------------------
// 通用，但有資安疑慮
// $id = $_REQUEST["id"];
// $a = $_REQUEST["a"];
// $b = $_REQUEST["b"];
// echo $id;
// echo $a + $b; //數字的加總用 + ("10" + 10 他會把+左邊能運算的改成數字 = 20)
//("10" + null 他會把null自動預設值0 = 10)
// --------------------------
// $id = $_REQUEST["id"];
// $a = $_REQUEST["a"];
// $b = $_REQUEST["b"];
// echo $a . $b; //文字的合併 . ("10" . 10 他會把+左邊能運算的改成數字 = 1010)
// ("10" . null 他會把null自動預設值空字串 = 10)

//AJAX
// $a = $_REQUEST["a"];
// $b = $_REQUEST["b"];
// echo $a + $b;

//表單複選處理
// $fruits = $_POST["fruits"];
// foreach ($fruits as $fruit) {
//     echo $fruit . "<br>";
// }

//上傳檔案
// $src = $_FILES['file']['tmp_name'];
// $dst = 'C:/Users/user/Desktop/' . $_FILES['file']['name'];
// if (move_uploaded_file($src, $dst) == 1) {
//     echo 'done';
// } else {
//     echo 'error code: ' . $_FILES['file']['error'];
// }

//字典
// $users = [];
// $users[count($users)] = ['name' => 'John', 'age' => 36];
// $users[count($users)] = ['name' => 'Mei', 'age' => 27];
// foreach ($users as $user) {
//     echo $user['name'] . ": " . $user['age'] . "\n";
// }

//陣列
//key value排序
// ksort 是按照key來排(下面例子key=name age score)
// krsort 是 反向排序 按照key來排(下面例子key=name age score)
// asort 是按照value來排(下面例子value=David 36 20)
// arsort 是 反向排序 按照value來排(下面例子value=David 36 20)
// $arr = ["name"=>"David","age"=>36,"score"=>20];

// ksort($arr);
// krsort($arr);
// asort($arr);
// arsort($arr);
// print_r($arr);

//JSON字串轉陣列
// $json = '[{"name":"David","age":36},{"name":"Mei","age":27}]';
// $arr = json_decode($json, true); //這一行就能JSON解析 要有那個true才可以變成陣列
// $arr[0]["sex"]="M"; //直接新增一筆到陣列 !!很重要
// print_r($arr);

// echo $arr[0]["name"];

//陣列轉JSON字串
// $arr = [
//     ["name"=> "王大明","age"=> 36],
//     ["name"=> "李明","age"=> 56]
// ];

// $str = json_encode($arr, JSON_UNESCAPED_UNICODE); //加上JSON_UNESCAPED_UNICODE才不會被轉成Unicode

// var_dump($str);

//模組化程式設計
//把陣列存到main.php檔案，再用require讀取進來
// require 遇到錯誤後停止
// include 遇到錯誤後繼續
// require_once() include_once() 加上once有重複就不會有錯誤
// 建議用require()
// require "main.php";

// $str = json_encode($arr, JSON_UNESCAPED_UNICODE); 

// var_dump($str);

//函數function
// require "main.php";
// echo f("30", "20");

// 變數範圍
// function test()
// {
//     global $n; //這一行會讓function裡面的$n從區域變全域
//     $n = 20; //區域變數
// };

// $n = 10; //全域變數
// test();
// echo $n; //n=10 改成全域後 n=20

// 不想用global，也可以用call-by-address
// function test(&$n){
//     $n = 20;
// };

// $n = 10;
// test($n);
// echo $n; //n=20

//靜態變數
//static這個變數只會在第一次時初始化，這個變數的生命週期到整個程式結束他才會被清理掉
//加上static記憶體就會在同一個位子，結束function也不會被回收掉
// function counter() {
//     static $n = 0;
//     $n += 1;
//     echo $n . "\n";
// }
// counter();
// counter();
// counter();

//Nullable型態
//資料型態前面可以加?，可以接受null或值
//PHP的情況null跟數字運算會變成0
//PHP的情況null跟字串運算會變成空字串
// function counter(?int $n){
//     echo $n * 3;
// }
// counter(null);

// Closure語法
// function counter($x)
// {
//     return $x(false);
// }

// $n = counter(function ($error) {
//     if (! $error) {
//     return 5 + 3;
//     } else {
//         return 20;
//     }
// });

// echo $n;



