<?php
// injection攻擊(打在form.php的input)
//' union all select * from userinfo; -- 

require "db.php";

$uid = $_REQUEST["uid"];
$pwd = $_REQUEST["pwd"];
//存取圖片
// $filename = $_FILES['file']['tmp_name'];
// // $image = file_get_contents($filename);

// //取出照片固定長寬比
// list($width, $height) = getimagesize($filename);
// $ratio = $width / $height;
// $newwidth = 800;
// $newheight = $newwidth / $ratio;
// //製作一個畫布存成JPG放回資料庫
// $src = imagecreatefromjpeg($filename);
// $dst = imagecreatetruecolor($newheight, $newheight);
// imagecopyresized($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
// ob_start();
// imagejpeg($dst);
// $image = ob_get_contents();
// ob_end_clean();
// ------------------------------------------


//防止資料隱碼攻擊(SQL injection) 參考講義的綁定變數
$sql = "select * from userinfo where uid = ? and pwd = ?";
$stmt = $mysqli->prepare($sql);
$stmt->bind_param("ss", $uid, $pwd); //變成兩個s，分別對應後面的參數($uid $pwd)
$stmt->execute();
$result = $stmt->get_result();
// -----------------------------------
//query()只能執行一個SQL comments 
//multi_query()才可以執行兩個SQL comments
// $result = $mysqli->query($sql);
// while ($row = $result->fetch_assoc()) {
//     echo $row["cname"] . "<br>\n";
// }

//multi_query()用法範例
// $sql = "select * from userinfo where uid = '{$uid}'; select * from house; insert into userinfo values ('A04', '王毛毛', null); select * from live";
// $mysqli->multi_query($sql);
// while (true) {
//     if ($result = $mysqli->store_result()) {
//         while ($row = $result->fetch_row()) {
//             print_r($row);
//         }
//     }

//     if ($mysqli->more_results()) {
//         $mysqli->next_result();
//         printf("-------------\n");
//     } else {
//         break;
//     }
// }


// 存入圖片
// $sql = "update userinfo set image = ? where uid = ? and pwd = ?";
// $stmt = $mysqli->prepare($sql);
// $stmt->bind_param("bss", $image, $uid, $pwd);
// $stmt->send_long_data(0, $image);
// $stmt->execute();
// //跳轉到showimage.php檔繼續
// die("<a href=\"showimage.php?uid={$uid}\">show image</a>");
// $result = $stmt->get_result();


?>

<html>

<head>
    <meta charset="utf-8">
</head>

<body>
    <table border="1">
        <tr>
            <th>帳號</th>
            <th>姓名</th>
        </tr>
        <?php while ($row = $result->fetch_assoc()) { ?>
            <tr>
                <td><?php echo $row["uid"]; ?></td>
                <td><?php echo $row["cname"]; ?></td>
            </tr>
        <?php } ?>
    </table>
</body>

</html>