<?php
$data = file_get_contents("data.txt");
$arr = explode("\n", $data);
unset($arr[count($arr)-1]);

echo "<pre>";
print_r($arr);
echo "</pre>";

$arr[1] = "I have a pen";
echo "<hr>";
echo "<pre>";
print_r($arr);
echo "</pre>";

$s = "";
foreach($arr as $el) {
    $s .= $el . "\n";
}

file_put_contents("data.txt", $s);
?>