<?php
//物件導向

//Person是"物件"
class Person
{
    //$name $hairColor 這個變數專有名詞叫"屬性property" 另一個名子"成員變數 member variable"
    //PHP 8 下面這兩行public 可以不用寫， 當下面function出現$this->name時，會自動生成public $name = '';
    public $hairColor = '';
    public $name = '';

    //readonly string 只能讀取不能更改
    // public readonly string $hairColor;

    //public 在class外面可以存取，private 只能在class內部操作
    // private $name = '';
    //要寫成function才能在外部使用(不建議這樣做)
    function getName()
    {
        return $this->name;
    }

    //叫物件去做事的function專有名詞較"method" 另一個名子叫"成員函數 member function"
    function sendMail()
    {
        //操作成員變數，前面一定加this，如果成員變數加上錢字號，$name就會變區域變數
        echo "{$this->name}: send mail done\n";
    }

    //兩個PHP特殊function，名子不能換，都不能return
    //建構子函數(重要)，可以加參數
    function __construct($name, $color)
    {
        $this->name = $name;
        $this->hairColor = $color;
    }
    
    //如果上面public那隱藏的話又想用private跟readonly就可以像下面這樣寫
    // function __construct(private string $name, public readonly string $color)
    // {
    //     $this->name = $name;
    //     $this->hairColor = $color;
    // }

    //解構子函數(不太重要)，不能加參數
    function __destruct()
    {
        echo "{$this->name}: will do garbage collection\n";
    }
}

//實體化 電腦會分一個記憶體出來裝實體化的東西
//$tom 是"實體變數 Instance"
//Person裡面的參數Tom, black 是帶入建構子函數的參數
$tom = new Person('Tom', 'white');
// ----------------------------
// $tom->hairColor = 'green';
echo $tom->hairColor . "\n";
$tom->sendMail();

// $betty = new Person("Betty2", "red");
// $betty->sendMail();


// -------------------------------------------------



//繼承
//盡量單一繼承，不要多重繼承
class NewPerson extends Person {
    function fly(){
        echo "{$this->name} can fly\n";
    }

    //複寫
    //function名稱跟參數都必須跟父類別一樣才可以複寫
    function sendMail() {
        echo "new send mail\n";
    }

    //呼叫父類別的sendMail
    function parentSendMail() {
        parent::sendMail();
    }
}

$betty = new NewPerson("Betty2", "red");
// $betty->fly();
// $betty->sendMail();
$betty->parentSendMail();