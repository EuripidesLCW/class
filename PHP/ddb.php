<?php
class ddb {
    private static $mysqli;
    //連線MySQL
    function __construct() {
        
        $host = "127.0.0.1";
        $user = "root";
        $pwd = "";
        $db = "addressbook";
        
        ddb::$mysqli = new mysqli($host, $user, $pwd, $db);
    }

    //處理select裡面的bind_param($types, ...$params)
    private static function getTypes($params) {
        $types = "";
        foreach($params as $param) {
            switch(gettype($param)) {
                case "string":
                    $types .="s";
                    break;
                case "integer":
                    $types .="i";
                    break;
                case "double":
                    $types .="f";
                    break;
            }
        }
        return $types;
    }

    //select
    // static function select($sql, ?Array $params=null) {
    //     if($params == null) {
    //         $result = ddb::$mysqli->query($sql);
    //     } else {
    //         $types = ddb::getTypes($params);
    //         $stmt = ddb::$mysqli->prepare($sql);
    //         $stmt->bind_param($types, ...$params);
    //         $stmt->execute();
    //         $result = $stmt->get_result();
    //     }
        
    //     $rows = $result->fetch_all(MYSQLI_ASSOC);
    //     return $rows;
    // }

    private static function query($sql, ?Array $params=null) {
        if($params == null) {
            // $result = ddb::$mysqli->query($sql);
            
            //解法1
            // $result = ddb::$mysqli->query($sql);
            // while (ddb::$mysqli->next_result()) {

            // }

            //解法2(推薦這個，因為query原本只能傳回一個，現在會傳回兩個，所以不如用prepare參數綁定)
            $stmt = ddb::$mysqli->prepare($sql);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();

        } else {
            
            $types = ddb::getTypes($params);
            $stmt = ddb::$mysqli->prepare($sql);
            $stmt->bind_param($types, ...$params);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
        }
        
        return $result;
    }
    
    static function select($sql, $completion, ?Array $params=null) {
        $result = ddb::query($sql, $params);
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        $completion($rows);
    }
    
    static function insert($sql, ?Array $params=null) {
        return ddb::query($sql, $params);
    }
    
    static function update($sql, ?Array $params=null) {
        return ddb::query($sql, $params);
    }
    
    static function delete($sql, ?Array $params=null) {
        return ddb::query($sql, $params);
    }
    
}

$db = new ddb();
// $rows = ddb::select("select * from bill where tel = ? and fee = ?", ['1111', 300]);
// foreach($rows as $row) {
//     echo $row["tel"] . ": " . $row["fee"] . "\n";
// }
?>