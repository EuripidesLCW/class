<?php
class Userinfo {
    function __construct($db){
        $this->db = $db;
    }

    function login($uid, $pwd, $completion) {
        $this->db->select("call login('{$uid}', '{$pwd}')", function($rows){
            $this->token = $rows[0]["token"];
            if ($this->token != null) {
                $this->db->select("select * from userinfo where token = ?", function ($rows){
                    $this->cname = $rows[0]["cname"];
                }, [$this->token]);
            }
        });
        
        $completion($this->token);
    }
}


?>