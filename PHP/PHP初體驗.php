<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<!-- --------------------------------- -->
<!-- 這樣就能由後端產出 -->
<?PHP
echo "<script>\n";
echo "alert('hi')\n";
echo "</script>\n";
?>
<!-- --------------------------------- -->

<body>
    <?php
    echo "Hello, World\n";
    ?>

    <!-- 只有一行的話 -->
    <?= "Hello, World!" ?>
</body>

</html>