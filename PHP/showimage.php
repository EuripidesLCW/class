<?php
//取得圖片
require("db.php");
$uid = $_REQUEST["uid"];

$stmt = $mysqli->prepare("select * from userinfo where uid = ?");
$stmt->bind_param("s", $uid);
$stmt->execute();
$result = $stmt->get_result();
$row = $result->fetch_assoc();
$data = $row["image"];

// header('content-type: image/jpeg');
// echo $data;

//放到image標籤
//抓檔案的type
$mime_type = (new finfo(FILEINFO_MIME_TYPE))->buffer($data);
//轉成base64編碼
$data_base64 = base64_encode($data);

//base64可以把圖片轉成字串(很常用)
$src = "data: image/{$mime_type}; base64,{$data_base64}";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p><?= $row["cname"]?></p>
    <img src="<?= $src ?>">
</body>
</html>