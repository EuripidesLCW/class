import React from 'react';
 
import { NavLink } from 'react-router-dom';
 
const Navigation = () => {
    return (
       <div>
          <NavLink to="/">首頁</NavLink>&nbsp;|&nbsp;
          <NavLink to="/about">關於我們</NavLink>&nbsp;|&nbsp;
          <NavLink to="/contact">聯絡資訊</NavLink>
       </div>
    );
}
 
export default Navigation;