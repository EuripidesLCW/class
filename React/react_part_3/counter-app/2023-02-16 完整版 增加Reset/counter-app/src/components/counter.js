import React, { Component } from 'react';

class Counter extends Component {
    render() { 
        // console.log(this.props);
        return (
        <div>
            <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
            <button id="btn"
                className='btn btn-outline-success btn-sm'
                onClick={  () => { this.props.onIncrement(this.props.order)  }  }
            >Increment</button>
            <button 
                className="btn btn-outline-danger btn-sm m-1"
                onClick={ () => { this.props.onDelete(this.props.order) } }
                >
                Delete
            </button>
        </div>
        )
    }

    getBadgeClasses() {
        var classes = "badge m-2 badge-";
        classes += (this.props.value === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        return this.props.value === 0 ? "Zero" : this.props.value;
    }
}
 
export default Counter;
