import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {
    state = { 
        counters: [
            {id: 1, value: 0},
            {id: 2, value: 1},
            {id: 3, value: 2},
            {id: 4, value: 3}
        ]
    } 
    render() { 

        return (<div>
            <button 
                className="btn btn-outline-warning btn-md"
                onClick={this.doReset}
                >
                    Reset
            </button>
            {
                this.state.counters.map( 
                    (c, index) => <Counter 
                        key = {c.id} 
                        value = {c.value} 
                        id = { c.id }
                        order = { index }
                        onDelete = { this.doDelete }
                        onIncrement = { this.doIncrement }
                    />
                )
            }
        </div>);
    }
    doDelete = (deleteIndex) => {
        var newState = {...this.state};
        newState.counters.splice(deleteIndex, 1);
        this.setState(newState);
        
    }
    // doDelete = (deleteId) => {
    //     var newState = {...this.state};
    //     newState.counters = newState.counters.filter( c => c.id !== deleteId );
    //     this.setState(newState);
        
    // }
    doReset = () => {
        console.log(this.state.counters);
        var newState = {...this.state};
        for (let i = 0; i < this.state.counters.length; i++) {
            newState.counters[i].value = 0;
        }
        this.setState(newState);
        
    }
    doIncrement = (order) => {
        var newState = {...this.state};
        newState.counters[order].value += 1;
        this.setState(newState);
    }
}
 
export default Counters;