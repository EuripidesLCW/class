import React, { Component } from 'react';
import Counter from './counter';

class counters extends Component {
    state = {
        counters: [
            { id: 1, value: 4 },
            { id: 2, value: 0 },
            { id: 3, value: 1 },
            { id: 4, value: 9 }
        ]
    }
    render() {
        return (
            <div>
                <button className="btn btn-warning" onClick={() => { this.doReset }}>Reset</button>
                {this.state.counters.map(
                    c => <Counter
                        key={c.id}
                        value={c.value}
                        id={c.id}
                        order={index}
                        onDelete={this.doDelete}
                        onIncrement={this.doIncrement}
                    />)}
            </div>
            // counter元件的state要改 this.props.value
        );
    }

    // doDelete = (deletedId) => {
    //     let newState = { ...this.state };
    //     newState.counters = newState.counters.filter(x => x.id !== deletedId);
    //     this.setState(newState);
    // }

    doDelete = (deleteIndex) => {
        var newState = { ...this.state };
        newState.counters.splice(deleteIndex, 1);
        this.setState(newState);

    }

    doReset = () => {
        console.log(this.state.counters);
        var newState = { ...this.state };
        for (let i = 0; i < this.state.counters.length; i++) {
            newState.counters[i].value = 0;
        }
        this.setState(newState);

    }
    doIncrement = (order) => {
        var newState = { ...this.state };
        newState.counters[order].value += 1;
        this.setState(newState);
    }
}

export default counters;