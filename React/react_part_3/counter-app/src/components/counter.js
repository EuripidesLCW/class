import React, { Component } from 'react';

class counter extends Component {
    state = {
        count: this.props.value
    }

    render() {
        return (
            <div>
                <span className={this.getBadgeclasses()}>{this.formatCount()}</span>
                <button className="btn btn-success" onClick={this.handleIncrement}>Increment</button>
                <button className="btn btn-danger m-3" onClick={() => { this.props.onDelete(this.props.id) }}>Delete</button>
            </div>
        )
    }

    handleIncrement = (product) => {
        console.log(product)
        this.setState({ count: this.state.count + 1 })

        // 這種寫法也可以，修改值回傳回去
        // let newState = {...this.state};
        // newState.count++;
        // this.setState(newState);
    }

    getBadgeclasses() {
        let classes = "badge  m-3 badge-";
        classes += this.state.count === 0 ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        const { count } = this.state;
        return count === 0 ? "Zero" : count;
    }
}

export default counter;