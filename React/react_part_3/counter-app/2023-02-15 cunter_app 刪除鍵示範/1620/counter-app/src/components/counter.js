import React, { Component } from 'react';

class Counter extends Component {
    state = { 
        count: this.props.value
    } 

    render() { 
        return (
        <div>
            <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
            <button id="btn"
                className='btn btn-outline-success btn-sm'
                onClick={  this.handleIncrement  }
            >Increment</button>
            <button 
                className='btn btn-outline-danger btn-sm'
                onClick={ () => { this.props.onDelete(this.props.id) }  }
            >Delete</button>
        </div>
        )
    }
    handleIncrement = () => {
        console.log(this.state.count);
        var newState = {...this.state};
        newState.count += 1;
        this.setState(newState);
        // this.setState({ count: this.state.count + 1 });
    }

    getBadgeClasses() {
        var classes = "badge m-2 badge-";
        classes += (this.state.count === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount() {
        // const count = this.state.count;
        const { count } = this.state;
        return count === 0 ? "Zero" : count;
    }
}
 
export default Counter;
