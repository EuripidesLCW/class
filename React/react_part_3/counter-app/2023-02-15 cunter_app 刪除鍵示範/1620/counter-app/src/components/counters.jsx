import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {
    state = { 
        counters: [
            {id: 1, value: 6},
            {id: 2, value: 1},
            {id: 3, value: 2},
            {id: 4, value: 3}
        ]
    } 
    render() { 

        return (<div>
            {
                this.state.counters.map( 
                    c => <Counter 
                            key={c.id} 
                            value={c.value} 
                            id={c.id}
                            onDelete={this.doDelete} 
                         />
                )
            }
        </div>);
    }

    doDelete = (deletedId) => {
        // alert("del: " + deletedId);
        let newState = {...this.state};
        newState.counters = newState.counters.filter( x => x.id !== deletedId );
        this.setState(newState);
    }
}
 
export default Counters;