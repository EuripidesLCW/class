import React, { Component } from 'react';
import axios from 'axios';

class TodoCreate extends Component {
    state = {
        todoItem: { todoTableId: 1, title: "", isComplete: 0 }
    }
    render() {
        return (
            <div className="container">

                <h1>待辦事項清單 - 新增</h1>
                <hr />
                <div className="row">
                    <div className="col-md-4">
                        <form action="/Todo/Create" method="post">

                            <div className="form-group">
                                <label className="control-label" htmlFor="Name">項目名稱</label>
                                <input className="form-control" type="text" id="Name"
                                    name="Name" value={this.state.todoItem.title}
                                    onChange={this.dotitleChange} />
                            </div>
                            <div className="form-group form-check">
                                <label className="form-check-label">
                                    <input className="form-check-input" type="checkbox" id="IsComplete"
                                        name="IsComplete" value="1"
                                        checked={this.state.todoItem.isComplete}
                                        onChange={this.doisCompleteChange} /> 是否已完工
                                </label>
                            </div>
                            <div className="form-group">
                                <input type="button"
                                    onClick={this.dobuttonChange} value="確定" className="btn btn-outline-primary" /> |
                                <a href="/Todo/Index" className="btn btn-outline-info">取消</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        );
    }

    dotitleChange = (e) => {
        let newState = { ...this.state };
        newState.todoItem.title = e.target.value;
        this.setState(newState);
    }

    doisCompleteChange = (e) => {
        let newState = { ...this.state };
        newState.todoItem.isComplete = e.target.checked; //!newState.todoItem.isComplete;
        this.setState(newState);
    }

    dobuttonChange = async (e) => {
        await axios.post("http://localhost:8000/todo/create", this.state.todoItem);
        window.location = "/";
    }

}

export default TodoCreate;