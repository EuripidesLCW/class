import React, { Component } from 'react';

class TodoIndex extends Component {
    state = {
        todoList: [
            { "todoTableId": 1, "title": "Job X", "isComplete": 1 },
            { "todoTableId": 2, "title": "Job Y", "isComplete": 0 },
            { "todoTableId": 3, "title": "Job Z", "isComplete": 1 }
        ]
    }
    render() {
        return (
            <div>
                <div class="container">

                    <h1>
                        待辦事項清單
                        <a href="/Todo/Create" class="btn btn-outline-success btn-md float-right">
                            新增
                        </a>
                    </h1>

                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>
                                    項目名稱
                                </th>
                                <th>
                                    是否已完工
                                </th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Job A
                                </td>
                                <td>
                                    <input class="check-box" disabled="disabled"
                                        type="checkbox" />
                                </td>
                                <td>
                                    <span class="float-right">
                                        <a href="/Todo/Edit/1" class="btn btn-outline-primary btn-sm">編輯</a> |
                                        <a href="/Todo/Delete/1" class="btn btn-outline-danger btn-sm">刪除</a>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>



                </div>
            </div>
        );
    }
}

export default TodoIndex;