<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\App;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/a', function () {
//     session(['test' => 'hello']);
//     Cookie::queue('name', 'David', 10);
//     return 'done';
// });

// Route::get('/b', function () {
//     return session('test');
// });

// Route::get('/b', ViewController::class);

// Route::get('/c', function () {
//     return Cookie::get('name');
// });

//PHP方法的語系
// Route::get('/en', function () {
//     App::setLocale('en');
//     return __('aaa.happy');
// });

// Route::get('/tw', function () {
//     App::setLocale('tw');
//     return __('aaa.happy');
// });

//JSON方法的語系
Route::get('/en', function () {
    App::setLocale('en');
    return __('welcome');
});

Route::get('/tw', function () {
    App::setLocale('tw');
    return __('welcome');
});