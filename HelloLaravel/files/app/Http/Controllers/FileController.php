<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileController extends Controller
{
    function form() {
        return view('form');
    }

    function process(Request $request) {
        if($request->hasFile('file1')) {
            $file1 = $request->file1;
            $original1 = $file1->getClientoriginalName();
            $filename1 = $file1->store('documents');
            echo $original1 . "<br>";
            echo $filename1 . "<br>";
        }

        if($request->hasFile('file2')) {
            $file2 = $request->file2;
            $original2 = $file2->getClientoriginalName();
            $filename2 = $file2->store('documents');
            echo $original2 . "<br>";
            echo $filename2 . "<br>";
        }
        return '<p>done';
    }
}
