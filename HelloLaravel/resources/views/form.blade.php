<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<script>
    window.onload = function() {
        let myform = document.getElementById("myform")

        document.getElementById("add").onclick = function() {
            myform.action = "run/add"
            myform.submit();
        }

        document.getElementById("minus").onclick = function() {
            myform.action = "run/minus"
            myform.submit();
        }

        document.getElementById("times").onclick = function() {
            myform.action = "run/times"
            myform.submit();
        }

        document.getElementById("div").onclick = function() {
            myform.action = "run/div"
            myform.submit();
        }
    }
</script>

<body>
    <form id="myform" action="run" method="post">
        <!-- 防止跨域攻擊可以打@csrf查看裡面的token，原理就是隱藏了一條input，裡面有個token要比對一致 -->
        @csrf

        <!-- 因為表單只能GET 跟 POST，可以用下面這hidden傳直進去(跨域攻擊手法) -->
        <input type="hidden" value="delete" name="method">
        <!-- Laravel可以寫下面這樣，@method('put'),put裡面可以改 -->
        @method('put')
        
        Number A: <input name='a'>
        Number B: <input name='b'>
        <button id="add">Add</button>
        <button id="minus">Minus</button>
        <button id="times">Times</button>
        <button id="div">Div</button>
    </form>
</body>

</html>