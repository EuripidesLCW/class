<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mymodel;

class TestController extends Controller
{
    private $model;
    //讓Mymodel實體化
    function __construct() {
        $this->model = new Mymodel();
    }

    function __invoke($name='Guest', $uid='A09') {
        return view('test', [
            'name' => $name,
            'uid' => $uid
        ]);
    }

    function add(Request $request) {
        $a = $request->a;
        $b = $request->b;
        $ans = $this->model->add($a, $b);

        return view('answer', [
            "a" => $a,
            "op" => "+",
            "b" => $b,
            "ans" => $ans
        ]);
    }

    function minus(Request $request) {
        $a = $request->a;
        $b = $request->b;
        $ans = $this->model->minus($a, $b);

        return view('answer', [
            "a" => $a,
            "op" => "-",
            "b" => $b,
            "ans" => $ans
        ]);
    }

    function times(Request $request) {
        $a = $request->a;
        $b = $request->b;
        $ans = $this->model->times($a, $b);

        return view('answer', [
            "a" => $a,
            "op" => "*",
            "b" => $b,
            "ans" => $ans
        ]);
    }

    function div(Request $request) {
        $a = $request->a;
        $b = $request->b;
        $ans = $this->model->div($a, $b);

        return view('answer', [
            "a" => $a,
            "op" => "/",
            "b" => $b,
            "ans" => $ans
        ]);
    }


}
