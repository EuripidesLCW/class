<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Userinfo;
use App\Http\Requests\MyFormRequest;

class DatabaseController extends Controller
{
    function insert(Request $request)
    {
        $user = new Userinfo();
        $user->uid = $request->uid;
        $user->cname = $request->cname;
        $user->save();
        return 'done';
    }

    // function query(Request $request) {
    //     // $users = Userinfo::where('uid', $request->uid)->get();
    //     // Userinfo如果有設定主索引，就可以用下面的方發find
    //     // 但因為只會回傳一個，下面已經寫死陣列，就乾脆用陣列包著，讓他變成陣列，下面就不用修改了
    //     $users = [Userinfo::find($request->uid)];
    //     return view('formquery', [
    //         'users' => $users
    //     ]);
    // }


    //驗證
    function query(MyFormRequest $request)
    {
        $request->validated();

        $users = [Userinfo::find($request->uid)];
        return view('formquery', [
            'users' => $users
        ]);
    }
}
