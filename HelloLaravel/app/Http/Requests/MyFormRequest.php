<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Contracts\Service\Attribute\Required;

class MyFormRequest extends FormRequest
{
    //打錯後可以跳頁，$redirect名子不能改
    // protected $redirect = '/api/userinfo';
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            // 'uid' => 'Required|max:5'

            'uid' => ['required', 'confirmed']

            // 'uid' => 'unique:Userinfo,uid'
        ];
    }
    //錯誤訊息可以改，function名稱一定要叫messages()
    public function messages() {
        return [
            'uid.required' => ':attribute 必填',
            'uid.max' => ':attribute 不能超過:max個字',
            'uid.unique' => '重複啦~',
            'uid.confirmed' => '輸入不一致'
        ];
    }
}
