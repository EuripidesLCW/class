<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mymodel extends Model
{
    use HasFactory;

    function add($a, $b) {
        return $a + $b;
    }

    function minus($a, $b) {
        return $a - $b;
    }
    
    function times($a, $b) {
        return $a * $b;
    }
    
    function div($a, $b) {
        return $a / $b;
    }
}
