<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class House extends Model
{
    use HasFactory;
    protected $table = 'House';
    protected $primaryKey = 'hid';
    protected $keyType = 'integer';
    public $timestamps = false;

    public function own(): HasMany {
        return $this->hasMany(Phone::class, 'hid', 'hid');
    }
}
