<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Userinfo extends Model
{
    use HasFactory;
    protected $table = 'userinfo';
    //只能一個主索引，複合索引不可以
    protected $primaryKey = 'uid';
    //不可以寫varchar，PHP不認識，所以要寫string
    protected $keyType = 'string';
    public $timestamps = false;

    public function lives(): HasManyThrough
    {
        //順序(House::class, Live::class, 'Live.uid', 'House.hid', 'Userinfo.uid', 'Live.hid')
        return $this->hasManyThrough(House::class, Live::class, 'uid', 'hid', 'uid', 'hid');
    }

    public function new_lives(): BelongsToMany {
        return $this->belongsToMany(House::class, 'live', 'uid', 'hid');
    }
}
