<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Userinfo;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/userinfo', function () {
    $users = Userinfo::select('uid', 'cname')->get();
    $json = $users->toJson(JSON_UNESCAPED_UNICODE);
    return response($json)
                ->header('content-type', 'application/json')
                ->header('charset', 'utf-8');
});
