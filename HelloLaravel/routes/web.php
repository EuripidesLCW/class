<?php

namespace App\Http\Controllers;

// use App\Http\Middleware\MyCheck;
use Illuminate\Support\Facades\Route;
use App\http\Middleware\ZeroCheck;
use App\Models\Userinfo;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\main;
use App\Models\House;
use App\Models\Phone;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// 範例1
// Route::get('/test/{name?}', function($name="Guest") {
//     return "Hello {$name}";
// });

// 範例2
// view第二個參數是陣列
// Route::get('/test/{name?}', function($name="Guest") {
//     // return view('test', ["name" => $name]);

//     //跟上面語法一樣
//     //兩個變數就寫兩個with()
//     return view('test') 
//             -> with("name", $name) 
//             -> with("uid", "A01");
// });

//新的語法
Route::get('/test/{name?}', TestController::class);

//連接form.blade.php
// Route::get('/form', function () {
//     return view('form');
// });

//也可以寫這樣
//只有一行，沒有要做什麼事情，就可以這樣寫
Route::view('/form', 'form');

Route::post('/run/add', [TestController::class, 'add']);
Route::post('/run/minus', [TestController::class, 'minus']);
Route::post('/run/times', [TestController::class, 'times']);
Route::post('/run/div', [TestController::class, 'div'])->middleware(ZeroCheck::class);

//middleware
//kernel.php檔案有設定，所以上面不用寫use
// Route::view('/form', 'form')->middleware('check');

Route::get('/list/{uid?}', function (?String $uid = null) {
    $rainy = true;
    if (isset($uid)) {
        $zoo = DB::select("select * from userinfo where uid = ?", [$uid]);
    } else {
        $zoo = DB::select("select * from userinfo");
    }

    return view('list', [
        'rainy' => $rainy,
        'zoo' => $zoo
    ]);
});

//連到內部不是blade的網頁(那個網頁建議放在public下面)
// Routo::get('/main', function () {
//     return File::get(public_path() . '/main.php');
// });

//連到外部網頁
Route::get('/go', function () {
    return redirect()->away("https:www.google.com");
});

//錯誤處理
// Route::get('/trans', function () {
//     try {
//         DB::transaction(function () {
//             DB::delete("delete from Live");
//             DB::insert("insert into UserInfo (uid, cname) values ('A01', '吳小美')");
//         });
//     } catch (Throwable $e) {
//         report($e);
//         abort(503);
//     }
// });

// Query Builder 只能查詢，可以讓SQL語法看起來更優雅，要用不用見仁見智
// Route::get('/query', function () {
//     $users = DB::table('userinfo')->dd()
//                 ->join('live', 'userinfo.uid', '=', 'live.uid')
//                 ->join('house', 'live.hid', '=', 'house.hid')
//                 ->where('userinfo.uid', 'A03')            
//                 ->get();
//     return $users[0]->address;
// });

//除錯用
// Route::get('/query', function () {
//     DB::table('userinfo')->get()->dd();
//     return;
// });

Route::get('/query', function () {
    DB::table('userinfo')->dump();
    return;
});

//Eloquent 要不要用見仁見智，好處是可以不用懂資料庫
// Route::post('/insert', [DatabaseController::class, 'insert']);
// Route::view('/forminsert', 'forminsert');


// Route::view('/formquery', "formquery");
// Route::get('/query', [DatabaseController::class, 'query']);

Route::get('/', function () {
    // $lives = Userinfo::find('A01')->lives;
    // foreach($lives as $live) {
    //     echo $live->address . '<br>';
    // }



    // foreach(House::find(1)->own as $phone) {
    //     echo $phone->tel . "<br>";
    // }

    //讓A04住進五號屋子
    $a04 = Userinfo::find('A04');
    $hid4 = House::find(5);
    $a04->new_lives()->save($hid4);

    $tel4444 = new Phone();
    $tel4444->tel = '4444';
    $hid4->own()->save($tel4444);
});


//驗證
Route::view('/formquery', "formquery");
Route::get('/query', [DatabaseController::class, 'query']);

//選uid印出來地址電話
Route::get('/query/{uid}', function ($uid) {
    $user = Userinfo::find($uid);
    echo $user->cname . '<br>';
    $houses = $user->new_lives;
    foreach($houses as $house) {
        echo $house->address . '<br>';
        $phones = $house->own;
        foreach($phones as $phone) {
            echo $phone->tel . '<br>';
        }
    }

});